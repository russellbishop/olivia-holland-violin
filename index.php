<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Olivia Holland, London Violinist</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#da532c">

    <link rel="stylesheet" type="text/css" href="/dist/css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,500" rel="stylesheet">
</head>
<body>

<div>

  <header class="c-hero u-flex">
    <div class="o-container u-relative u-z-1 u-mv-auto u-color-white u-align-center u-owl-one">
      <h1>Olivia Holland</h1>
      <h2 class="u-uppercase">Violinist, London</h2>
    </div>
  </header>

  <div class="o-container u-owl-two u-pv-4rem">

    <div class="u-owl-two">
      <h3>Olivia is a British violinist based in London & the South&nbsp;East.</h3>
      
      <p style="font-size: 1.2rem; font-weight: 300;">Her extensive experience has seen her perform internationally as both an orchestral and chamber musician. <?php /* <a href="#">Read Olivia's Bio</a></p> */ ?>

      <p><a href="mailto:oliviadholland@gmail.com" class="c-button">Send an enquiry &rarr;</a></p>
    </div>

  </div>
    
  <!-- <hr /> -->

  <div style="background-color: rgba(82, 82, 82, 0.05);">
  
    <div class="o-container" style="padding: 2rem 0;">

      <p class="u-h3">Olivia is available for bookings:</p>
    
      <ul class="u-h3">
        <li>Session Music</li>
        <li>Orchestral</li>
        <li>Weddings</li>
      </ul>

    </div>

  </div>

  <article class="o-container u-pv-4rem">

    <h3>Olivia's Biography</h3>

    <p>Olivia Holland is a British violinist based in London & the South East. Having graduated from the Trinity Laban Conservatoire of Music & Dance with first-class honours, her playing has been described as "intensive & passionate, with excellent panache & character".</p>

    <!-- notable venues across the UK and Europe where Olivia has performed include the Royal Albert Hall, Cadogan Hall, Fairfield Halls, La Madeleine in Paris and the Slovak Radio Building in Bratislava. -->

    <hr />

    <p class="u-h3">Live Performance</p>

    <details>
    <summary>XOYO: Re:imagine</summary>
    <p>Having performed over 100 sold out shows at XOYO & Jazz Cafe, Olivia is an active member of the string section for Re:imagine, "a London based events series that brings to life music's defining moments and iconic recordings with high production live renditions". Shows include renditions of Notorious B.I.G, Fleetwood Mac, Daft Punk, Madonna, Dr Dre & Kanye to name a small selection. Olivia also performed in the live orchestral rendition of the Stranger Things soundtrack to a sold out Electric Brixton in October 2017.</p>
    </details>

    <details>
    <summary>Alex Mendham Orchestra</summary>
    <p>Olivia has played with the Alex Mendham Orchestra as part of his violin section, who "perform the sophisticated and stylish music of the 1920’s and 30s with the modern energy and passion of their youthful years"; venues Olivia has performed in with the orchestra include The Ritz London, Cheltenham Town Hall & Petworth Music Festival.</p>
    </details>

    <details>
    <summary>Daniel Herskedal</summary>
    <p>In November 2015, Olivia performed alongside Daniel Herskedal in his string quartet as part of the EFG London Jazz Festival at King's Place. Previous years have included working with esteemed musicians from British band Empirical to perform songs from their album 'Tabula Rose' as part of Trinity Laban's CoLab Festival.</p>
    </details>

    <details>
    <summary>Sam Jewison Orchestra</summary>
    <p>She also holds the 2nd violin seat with the Sam Jewison Orchestra, with whom she has played two sold out shows at Camden's famous Jazz Cafe and multiple concerts across London.</p>
    </details>

    <details>
    <summary>Bring Me The Horizon & Alter Bridge</summary>
    <p>She is a member of the Parallax Orchestra, and with them has performed shows with bands including Bring Me The Horizon & Alter Bridge.</p>
    </details>

    <details>
    <summary>Live Bands</summary>
    <p>Session work has included performing with electronic bands such as Aquilo, <a href="http://www.phoriamusic.com/">Phoria</a> and Mt. Wolf, and <a href="http://thecoveryard.co.uk/">'The Coveryard Sessions'</a> who combine emerging artists with a live chamber orchestra.</p>
    </details>

    <details>
    <summary>Electric Violin</summary>
      <p>Olivia is also an electric violinist, performing with Euphonica Live as a member of their DJ Orchestra; she has played as part of a launch event for Peroni and recently performed at the Victoria and Albert museum around the ‘Opera: Passion Power and Politics’ exhibition.</p>

      <p>Olivia regularly performs on electric violin at Chino Latino for their live music nights at Park Plaza, River Bank.</p>
    </details>

    <hr />

    <p class="u-h3">Television Appearances</p>

    <details>
    <summary>ITV Britain's Got Talent Final 2018</summary>
    <p>Performing <a href="https://www.youtube.com/watch?v=XlAe2DXsiUw">Perfect Symphony by Ed Sheeran and Andrea Bocelli with Gruffydd Wyn</a>.<p>
    </details>

    <details>
    <summary>MTV EMA 2017</summary>
    <p>Television performances include the 2017 EMA with Parallax Orchestra performing with Clean Bandit, Zara Larsson, Julia Michaels, Anne-Marie & Eminem, </p>
    </details>

    <details>
    <summary>ITV X Factor 2017</summary>
    <p>Performing <a href="https://www.youtube.com/watch?v=chxLix8y0AI">Hero by Enruique Iglesias with Lloyd Macey</a>.</p><!-- EDIT -->
    </details>
    
    <details>
    <summary>ITV X Factor 2016</summary>
    <p>Performing <a href="https://www.youtube.com/watch?v=XlAe2DXsiUw">Impossible by Shontelle with Sam Lavery</a>.<p>
    </details>
    

    <hr />

    <p class="u-h3">Classical Performance</p>
    
    <details>
    <summary>Trinity Laban</summary>
    <p>Her passion for orchestral playing has seen her hold many principle positions with the Trinity Laban Sinfonia, Symphony Orchestra & String Ensemble.</p>
    </details>

    <details>
    <summary>Orchestra Vitae</summary>
    <p>Olivia has regularly performed with <a href="http://orchestravitae.co.uk/">Orchestra Vitae</a> since 2014.</p>
    </details>

    <details>
    <summary>Competitions</summary>
    <p>Olivia's performance of Schubert's Violin Sonata in G Minor (Sonatina) earned her a place in the final of the Leonard Smith & Felicity Young Duo Competition with pianist Claire Habbershaw in 2017.</p>
    </details>
    
    <details>
    <summary>Holland Quartet</summary>
    <p>Olivia leads The Holland Quartet, a versatile all female string quartet comprised of some of London's most captivating talent. Throughout 2017 they held a residency at 67 Pall Mall.</p>
    </details>

    <hr />

    <p class="u-h3">Session Recordings</p>

    <p>Olivia is an active session musician, performing regularly with string quartets & trios both classically and commercially across the UK. She is the 1st violinist of her string quartet The Holland Quartet, with whom she holds a residency at 67 Pall Mall.</p>

    <ul>
    <li>Bring Me The Horizon - Amo LP (Release January 2019)</li>
    <li>Ocean Wisdom - <a href="https://www.youtube.com/watch?v=K4NG5U58yOw">BBC Live Lounge</a></li>
    <li>Skinshape – Skinshape LP (Release October 2018)</li><!-- EDIT -->
    <li>Tom Ward-Thomas - <a href="https://www.youtube.com/watch?v=x-Tc1krV0_Y&feature=youtu.be">A Silent Night</a> (EP)</li>
    <li>British Film Institute Recording at Abbey Road</li>
    </ul>

    <!-- <p>Olivia plays on a 1976 William Luff violin & a white Aquila electric violin by Bridge Violins.</p> -->

  </article>
    
</div>

</body>
</html>
